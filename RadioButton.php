<?php
/**
 * PHPExcel - Excel data import to MySQL database script example
 * ==============================================================================
 * 
 * @version v1.0: PHPExcel_excel_to_mysql_demo.php 2016/03/03
 * @copyright Copyright (c) 2016, http://www.ilovephp.net
 * @author Sagar Deshmukh <sagarsdeshmukh91@gmail.com>
 * @SourceOfPHPExcel https://github.com/PHPOffice/PHPExcel, https://sourceforge.net/projects/phpexcelreader/
 * ==============================================================================
 *
 */
 
require 'Classes/PHPExcel/IOFactory.php';
//include ('productList.php');

// Mysql database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "Data_Upload_Utility";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
if(isset($_POST["Import"])){


 //Getting file  values 
 ($_FILES['file']);
 //$errors= array();
 $file_name = $_FILES['file']['name'];
 $file_tmp =$_FILES['file']['tmp_name'];

	//$inputfilename = 'example_file.xlsx';
$exceldata = array();




//  Read your Excel workbook
try
{
    $inputfiletype = PHPExcel_IOFactory::identify($file_tmp);
    $objReader = PHPExcel_IOFactory::createReader($inputfiletype);
    $objPHPExcel = $objReader->load($file_tmp);
}
catch(Exception $e)
{
    die('Error loading file "'.pathinfo($file_name,PATHINFO_BASENAME).'": '.$e->getMessage());
}
$selected_val = $_POST['data'];

//$selected_val2 = $_POST['yes'];
//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 

$highestColumn = $sheet->getHighestColumn();

//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++)
{ 
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	/*$sql = "INSERT INTO ProductList (Submitter_Name, Product_ID, Brand_Name ,Brand_Sub_Type_Name, Product_Type,TPD_Compliance)
			VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."' , '".$rowData[0][3]."' , '".$rowData[0][4]."', '".$rowData[0][5]."')";
			//$sql = "INSERT INTO withdrawlist (Submitter_Name, Product_ID, Brand_Name ,Brand_Sub_Type_Name, Product_Type)
			//VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."' , '".$rowData[0][3]."' , '".$rowData[0][4]."')";
	
	if (mysqli_query($conn, $sql)) {
		$exceldata[] = $rowData[0];
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}*/
	if($selected_val == 'product'){
    //  Insert row data array into your database of choice here
   // $queryExecuted = 'SELECT * FROM productlist';
   // if(mysql_num_rows($queryExecuted) > 0) {
	//$sql = "REPLACE INTO productlist(Submitter_Name, Product_ID, Brand_Name ,Brand_Sub_Type_Name, Product_Type,TPD_Compliance, country)
//VALUES('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."' , '".$rowData[0][3]."' , '".$rowData[0][4]."', '".$rowData[0][5]."','UK' )";	
    //$sql = "BEGIN TRAN
    //DELETE  productlist 
    //INSERT INTO productlist (Submitter_Name, Product_ID, Brand_Name ,Brand_Sub_Type_Name, Product_Type,TPD_Compliance, country)
//			VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."' , '".$rowData[0][3]."' , '".$rowData[0][4]."', '".$rowData[0][5]."','UK')
 //   COMMIT TRAN";
 //$queryExecuted = 'SELECT * FROM prodcutlist';
//if(mysql_num_rows($queryExecuted) > 0) { 
  
//}
//else if ($queryExecuted) > null){
    // $d_sql  = "DELETE productlist";
    // if (mysqli_query($conn, $sql)) {
    //     //$exceldata[] = $rowData[0];
    //     echo "Data Deleted";
	// } else {
    
	// 	echo "Error: " . $sql . "<br>" . mysqli_error($conn) ;
	// }
    $sql = " 
    
    

     INSERT INTO productlist (Submitter_Name, Product_ID, Brand_Name ,Brand_Sub_Type_Name, Product_Type,TPD_Compliance, country)
    VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."' , '".$rowData[0][3]."' , '".$rowData[0][4]."', '".$rowData[0][5]."','UK' )
     ";

    
    
    //mysql_query("delete productlist");
        //echo "insert query run succcess";
	if (mysqli_query($conn, $sql)) {
		//$exceldata[] = $rowData[0];
	} else {
    
		//echo "Error: " ;//. $sql . "<br>" . mysqli_error($conn) ;
	}
    
}

    
    // $queryExecuted = 'SELECT * FROM productlist'
    // else if(mysql_num_rows($queryExecuted) > 0) 
    // { 
    //     $sql = "TRUNCATE productlist";

    // }
    

	 else if($selected_val == 'withdraw'){
		$sql = "INSERT INTO withdrawlist (Submitter_Name, Product_ID, Brand_Name ,Brand_Sub_Type_Name, Product_Type)
			VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."' , '".$rowData[0][3]."' , '".$rowData[0][4]."')";
	
	if (mysqli_query($conn, $sql)) {
		//$exceldata[] = $rowData[0];
	} else {
		//echo "Error: " ;//. $sql . "<br>" . mysqli_error($conn);
	}
	}
	
}  

}



// Print excel data
//echo "<table >";

 //  Insert row data array into your database of choice here
 //$selected_val = $_POST['data'];

//  $sql = "SELECT * FROM productlist LIMIT 15 ";
//  $result = mysqli_query($conn, $sql);  
//     if (mysqli_num_rows($result) >  0) {
//      echo "<div class='table-responsive'><table id='myTable' class='table table-striped table-bordered'>
//              <thead><tr>
//                           <th>Submitter_Name</th>
//                           <th>Product_ID</th>
//                           <th>Brand_Name</th>
//                           <th>Brand_Sub_Type_Name</th>
//                           <th>Product_Type</th>
//                          <th> TPD_Compliance</th>
// 						 <th> Country</th>
//                         </tr></thead><tbody>";
//      while($row = mysqli_fetch_assoc($result)) {
//          echo "<tr><td>" . $row['Submitter_Name']."</td>
//                    <td>" . $row['Product_ID']."</td>
//                    <td>" . $row['Brand_Name']."</td>
//                    <td>" . $row['Brand_Sub_Type_Name']."</td>
//                    <td>" . $row['Product_Type']."</td>
// 				    <td>" . $row['TPD_Compliance']."</td>
// 					<td>  UK </td>
                  
//                    </tr>";        
//      }
    
//      echo "</tbody></table></div>";
     
// } else {
//      echo "you have no records";
// }




// foreach ($exceldata as $index => $excelraw)
// {
// 	echo "<tr>";
// 	foreach ($excelraw as $excelcolumn)
// 	{
// 		echo "<td>".$excelcolumn."</td>";
// 	}
// 	echo "</tr>";
// }
// echo "</table>";
// }
mysqli_close($conn);
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
</head>
<body>
    <div id="wrap">
        <div class="container">
            <div class="row">
                <form  id ="form-id" class="form-horizontal" action = ""   method="post" name="upload_excel" enctype="multipart/form-data">
                    <fieldset>
                        <!-- Form Name -->
                        <legend>Form Name</legend>
						<div class="form-group">
							
						</div>
                        <!-- File Button -->
                        <div class="form-group">
							<input type= "radio" name= "data" value = "product" id= "pro" > Product </input>
							<input type= "radio" name= "data" value= "withdraw" id = "with" > Withdraw </input>
							<br>
                            <label class="col-md-4 control-label" for="filebutton">Select File</label>
                            <div class="col-md-4">
                                <input type="file" name="file" id="file" class="input-large">
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="singlebutton">Import data</label>
                            <div class="col-md-4">
                                <button type="submit" id="submit" name="Import" onclick  = "uploadMessage()" class="btn btn-primary button-loading" data-loading-text="Loading..." onclick = "pageChange()">Import</button><br>
                                <a href  = "withdrawProducts.php" target= '_blank'> WithDraw List </a><br>
                                <a href  = "productList.php" target= '_blank'> Product List </a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
           
        </div>
    </div>
</body>
<script>

function uploadMessage() {
    alert("Your File is Uploaded");
}

// $(document).ready(function(){
//   $("input[name='data']").change(function(){
//     if($("#pro").prop("checked", false)){
//       $("#form-id").attr("action", "productList.php")
//     }
//     else{
//       $("#form-id").attr("action", "withdrawProducts.php")
//     }
//   }
//  function pageChange()
//  {
//    alert('CLick')  
//     // document.getElementById('form_id').action = 'somethingelse';
//     var  pro = document.getElementById('pro').value ;
//     var  with = document.getElementById('with').value;
//     if(pro == 'product'){
//     //var  p = document.getElementById('form-id').action ;// 'productList.php';
//     location.assign("productList.php");
//        // p = 'productList.php';

//     }
//     else if(with == 'withdraw' ){
//         //let w =document.getElementById('form-id').action ;//= 'withdrawProducts.php' ;
//          //w = 'withdrawProducts.php';
//          location.assign("withdrawProducts.php");

//     }

//  }
 


</script>

</html>