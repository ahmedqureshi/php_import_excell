<?php
/**
 * PHPExcel - Excel data import to MySQL database script example
 * ==============================================================================
 * 
 
 * @version v1.0: PHPExcel_excel_to_mysql_demo.php 2016/03/03
 * @copyright Copyright (c) 2016, http://www.ilovephp.net
 * @author Sagar Deshmukh <sagarsdeshmukh91@gmail.com>
 * @SourceOfPHPExcel https://github.com/PHPOffice/PHPExcel, https://sourceforge.net/projects/phpexcelreader/
 * ==============================================================================
 *
 */
 
require 'Classes/PHPExcel/IOFactory.php';
//include 'pagination.php';


// Mysql database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "Data_Upload_Utility";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
if(isset($_POST["Import"])){
 //Getting file  values 
 ($_FILES['file']);
 //$errors= array();
 
 $file_name = $_FILES['file']['name'];
 $file_tmp =$_FILES['file']['tmp_name'];

	//$inputfilename = 'example_file.xlsx';
$exceldata = array();




//  Read your Excel workbook
try
{
    $inputfiletype = PHPExcel_IOFactory::identify($file_name);
    $objReader = PHPExcel_IOFactory::createReader($inputfiletype);
    $objPHPExcel = $objReader->load($file_name);
}
catch(Exception $e)
{
    die('Error loading file "'.pathinfo($file_name,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
//delete data before uploading
$sql = "TRUNCATE TABLE productlist";
if (mysqli_query($conn, $sql)) {
    echo "deleted success";
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}
//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++)
{ 
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
    
    


    //  Insert row data array into your database of choice here
	$sql = "INSERT INTO productlist (Submitter_Name, Product_ID, Brand_Name ,Brand_Sub_Type_Name, Product_Type,TPD_Compliance)
			VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."' , '".$rowData[0][3]."' , '".$rowData[0][4]."', '".$rowData[0][5]."')";
	
	if (mysqli_query($conn, $sql)) {
		$exceldata[] = $rowData[0];
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}
}

}


// Print excel data
//echo "<table >";

 //  Insert row data array into your database of choice here
 $sql = "SELECT * FROM productlist LIMIT 10 ";
 $result = mysqli_query($conn, $sql);  
    if (mysqli_num_rows($result) > 0) {
        ?>
          <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
<?php
     echo "<div class='table-responsive'><table id='myTable' class='table table-striped table-bordered'>
             <thead><tr class='header'>
                          <th>Submitter_Name</th>
                          <th>Product_ID</th>
                          <th>Brand_Name</th>
                          <th>Brand_Sub_Type_Name</th>
                          <th>Product_Type</th>
                         <th> TPD_Compliance</th>
                         <th>Country</th>
                         <th>Date</th>
                        </tr></thead><tbody>";
     while($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td>" . $row['Submitter_Name']."</td>
        <td>" . $row['Product_ID']."</td>
        <td>" . $row['Brand_Name']."</td>
        <td>" . $row['Brand_Sub_Type_Name']."</td>
        <td>" . $row['Product_Type']."</td>
        <td>" . $row['TPD_Compliance']."</td>
        <td>" . $row['country']."</td>
        <td>" . $row['date']."</td>
        </tr>";        

     }
   
     echo "</tbody></table></div>";
     
} else {
     echo "you have no records";
}
?>
<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<?php
mysqli_close($conn);
?>



<!DOCTYPE html>
<html lang="en">
<head>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
      
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
</head>
<style>
* {
  box-sizing: border-box;
}
td
{
    background-color: #90ee90;
}
#myInput {
  background-image: url('/css/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 18px;
}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>

<body>
    <div id="wrap">
        <div class="container">
            <div class="row">
                <form class="form-horizontal" action="" method="post" name="upload_excel" enctype="multipart/form-data">
                    <fieldset>
                        <!-- Form Name -->
                        <legend>Form Name</legend>
                        <!-- File Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="filebutton">Select File</label>
                            <div class="col-md-4">
                                <input type="file" name="file" id="file" class="input-large" multiple>
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="singlebutton">Import data</label>
                            <div class="col-md-4">
                                <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
           
        </div>
    </div>
</body>
</html>